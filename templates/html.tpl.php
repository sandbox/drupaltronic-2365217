<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 ie" xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 ie" xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 ie" xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <!--<![endif]-->
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
<!--[if lt IE 9]><script src="<?php print base_path() . drupal_get_path('theme', 'rubens') . '/js/html5.js'; ?>"></script><![endif]-->

<?php
    $bg_image = 'none';

    $theme_style = theme_get_setting('layout_style', 'rubens');
    if ($theme_style == 'boxed') {
      $classes .= ' boxed';
      $box_image = theme_get_setting('boxed_bg_img', 'rubens');

      if ($box_image != 'none') {
        $bg_image = 'url("' . base_path() . path_to_theme() . '/images/background-patterns/boxed/' . $box_image . '") !important';
      }

      $bg_color = theme_get_setting('boxed_bg_color', 'rubens');
    } else {

      $wide_image = theme_get_setting('wide_bg_img', 'rubens');
      if ($wide_image != 'none') {
        $bg_image = 'url("' . base_path() . path_to_theme() . '/images/background-patterns/wide/' . $wide_image . '") !important';
      }
    }
    ?>

    <style type="text/css">
      body{

        background-image: <?php print $bg_image; ?>;
        <?php if (!empty($bg_color) && $bg_color != 'none'): ?>
          background: <?php print $bg_color; ?> !important;
        <?php endif; ?>
      }
    </style>

  </head>

<body class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>

