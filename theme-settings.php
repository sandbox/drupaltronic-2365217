<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function rubens_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['rubens_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('rubens Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
   
$form['rubens_settings']['skin'] = array(
      '#type' => 'fieldset',
      '#title' => t('Skin settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

 $form['rubens_settings']['skin']['layout_style'] = array(
      '#type' => 'select',
      '#title' => t('Layout style'),
      '#options' => array(
          'wide' => t('Wide'),
          'boxed' => t('Boxed'),
      ),
      '#default_value' => theme_get_setting('layout_style', 'rubens'),
  );

$form['rubens_settings']['skin']['wide'] = array(
      '#type' => 'fieldset',
      '#title' => t('Wide settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  $wide_bg_img_options = array('none' => 'No background');

  $wide_bg_dir = drupal_get_path('theme', 'rubens') . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'background-patterns' . DIRECTORY_SEPARATOR . 'wide';
  $bgs = file_scan_directory($wide_bg_dir, '/.*\.png/');

  if (!empty($bgs)) {
    foreach ($bgs as $bg) {
      if (isset($bg->filename)) {
        $wide_bg_img_options[$bg->filename] = $bg->filename;
      }
    }
  }

 $form['rubens_settings']['skin']['wide']['wide_bg_img'] = array(
      '#title' => t('Background image pattern '),
      '#type' => 'select',
      '#default_value' => theme_get_setting('wide_bg_img', 'rubens'),
      '#options' => $wide_bg_img_options,
  );


 $form['rubens_settings']['skin']['boxed'] = array(
      '#type' => 'fieldset',
      '#title' => t('Boxed settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  $boxed_bg_img_options = array();
  $dir = drupal_get_path('theme', 'rubens') . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'background-patterns' . DIRECTORY_SEPARATOR . 'boxed';

  $files = file_scan_directory($dir, '/.*\.png/');

  if (!empty($files)) {
    foreach ($files as $file) {
      if (isset($file->filename)) {
        $boxed_bg_img_options[$file->filename] = $file->filename;
      }
    }
  }


$form['rubens_settings']['skin']['boxed']['boxed_bg_img'] = array(
      '#title' => t('Background image pattern '),
      '#type' => 'select',
      '#default_value' => theme_get_setting('boxed_bg_img', 'rubens'),
      '#options' => $boxed_bg_img_options,
  );

$form['rubens_settings']['skin']['boxed']['boxed_bg_color'] = array(
      '#type' => 'select',
      '#title' => t('Background color'),
      '#default_value' => theme_get_setting('boxed_bg_color', 'rubens'),
      '#options' => array(
         'none' => 'No background color',
 	  '#1693A5' => 'Theme color-Dark cyan',
          '#D9D9D9' => 'beige',
          '#262626' => 'dark',
          '#F25824' => 'orange',
          '#7EB01A' => 'green',
          '#4396BF' => 'blue',
          '#EB7F00' => 'light orange',
          '#E53C3C' => 'red',
          '#B28EB4' => 'purple',
          '#CE5C76' => 'violet',
      ),
  );

  $skins_options = array();

  $dir = drupal_get_path('theme', 'rubens') . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'colors';

  $files = file_scan_directory($dir, '/.*\.css/');

  if (!empty($files)) {
    foreach ($files as $file) {
      if (isset($file->filename)) {
        $skins_options[$file->name] = $file->filename;
      }
    }
  }



 

$form['rubens_settings']['slideshow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front Page Slideshow'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['rubens_settings']['slideshow']['slideshow_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show slideshow'),
    '#default_value' => theme_get_setting('slideshow_display','rubens'),
    '#description'   => t("Check this option to show Slideshow in front page. Uncheck to hide."),
  );
  $form['rubens_settings']['slideshow']['slide'] = array(
    '#markup' => t('You can change the description and URL of each slide in the following Slide Setting fieldsets.'),
  );
  $form['rubens_settings']['slideshow']['slide1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 1'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rubens_settings']['slideshow']['slide1']['slide1_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide1_head','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide1']['slide1_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide1_desc','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide1']['slide1_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide1_url','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 2'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rubens_settings']['slideshow']['slide2']['slide2_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide2_head','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide2']['slide2_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide2_desc','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide2']['slide2_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide2_url','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide3'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 3'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rubens_settings']['slideshow']['slide3']['slide3_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide3_head','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide3']['slide3_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide3_desc','rubens'),
  );
  $form['rubens_settings']['slideshow']['slide3']['slide3_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide3_url','rubens'),
  );
  $form['rubens_settings']['slideshow']['slideimage'] = array(
    '#markup' => t('To change the Slide Images, Replace the slide-image-1.jpg, slide-image-2.jpg and slide-image-3.jpg in the images folder of the theme folder.'),
  );
}
