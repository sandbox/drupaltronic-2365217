About Rubens Multipurpose responsive theme
===========================================

Rubens is a clean, responsive and flexible drupaltheme that uses the power of bootstrap 3 to its limits. 
The site uses html5 markup code and is build with drupal standards.It’s very lightweight and  speeding fast. This template has endless possibilities.
The clean design can be used for any type of website; business, corporate, portfolio, blog, products, etc. 
   

Features:
=========

- Elastic Sticky header menu (for desktops & pads-landscape) 
- Mobile Sidr menu (for pads-Portrait an mobile phones.) 
- Font Awesome - Glyphicons
- Elegant and Easy to Use
- Modern, Clean, Flat Design
- Cross Browser Compatible
- SEO Optimized
- Many Useful Elements 
- FlexSlider 
- Carousel  and Testimonials
- And Much More ...

Setup is fast and easy. No need to install extra modules or an install profile. The theme will work straight out of the box.

There are many elements you can customize:
------------------------------------------

- Use logo (and /or) sitename-siteslogan.
- Boxed or wide layout option.(for wide demo check out: http://factor7.be)
- For wide settings: choose content background pattern.
- For boxed settings: choose background image or choose a background color. 
- Set front page flexslider
- Custom Blog layout. comments and comment form.


Browser compatibility:
=====================
The theme has been tested on following browsers. IE7+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 7.x.x
For the mobile menu to work it is advised to install the Jquery update module

Images
=====================
All images included in the theme are self taken and free for use.See also license text.

Developed by
============
www.drupaltronic.com


